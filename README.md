# L4T Environment Setup

All-in-one `repo`-based environment setup manifest to get devs started on L4T projects.

### Initialize

Use the `repo` tool to clone the manifest and initialize L4T repos:

```bash
repo init https://gitlab.com/l4t-community/manifest.git --depth=1 --no-clone-bundle
```

Use branches to init only what you need

```bash
repo init https://gitlab.com/l4t-community/manifest.git -b maruos --depth=1 --no-clone-bundle
```

### Sync

Sync up local sources with the selected branch(es)

```bash
repo sync --force-sync -j{CONNECTIONS}
```

### Contributing

Submit general suggestions for L4T projects under this repository, or specific suggestions under the relevant repo. Merge Requests are welcome on any of our repos.
